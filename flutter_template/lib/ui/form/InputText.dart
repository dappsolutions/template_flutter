import 'package:flutter/material.dart';

class InputText extends StatelessWidget {
  bool secureText;
  IconData prefixIcon;
  InputText({this.secureText = false, this.prefixIcon = Icons.book});

  @override
  Widget build(BuildContext context) {
    return TextField(
        obscureText: this.secureText,
        decoration: InputDecoration(
            border: OutlineInputBorder(),
            prefixIcon: Icon(this.prefixIcon)
        ),
    );
  }
}
