import 'package:flutter/material.dart';

class FloatingBtnPrimary extends StatelessWidget {
  IconData icons;
  Function onPressed;
  FloatingBtnPrimary({this.icons, this.onPressed});

  @override
  Widget build(BuildContext context) {
    return FloatingActionButton(
      child: Icon(this.icons),
      backgroundColor: Colors.blue,
      onPressed: this.onPressed,
    );
  }
}
