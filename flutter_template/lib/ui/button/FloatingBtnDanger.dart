import 'package:flutter/material.dart';

class FloatingBtnDanger extends StatelessWidget {
  IconData icons;
  Function onPressed;
  FloatingBtnDanger({this.icons, this.onPressed});

  @override
  Widget build(BuildContext context) {
    return FloatingActionButton(
      child: Icon(this.icons),
      backgroundColor: Colors.blue,
      onPressed: this.onPressed,
    );
  }
}
