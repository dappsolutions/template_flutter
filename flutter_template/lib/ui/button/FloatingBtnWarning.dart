import 'package:flutter/material.dart';

class FloatingBtnWarning extends StatelessWidget {
  IconData icons;
  Function onPressed;
  FloatingBtnWarning({this.icons, this.onPressed});

  @override
  Widget build(BuildContext context) {
    return FloatingActionButton(
      child: Icon(this.icons),
      backgroundColor: Colors.blue,
      onPressed: this.onPressed,
    );
  }
}
