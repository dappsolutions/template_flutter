import 'package:flutter/material.dart';

class ButtonPrimary extends StatelessWidget {
  String text;
  Function onPressed;
  ButtonPrimary({this.text = "BUTTON", this.onPressed});

  @override
  Widget build(BuildContext context) {
    return RaisedButton(
        child: Text(this.text),
        color: Colors.blue,
        onPressed: this.onPressed);
  }
}
