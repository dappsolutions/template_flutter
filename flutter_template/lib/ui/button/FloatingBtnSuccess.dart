import 'package:flutter/material.dart';

class FloatingBtnSuccess extends StatelessWidget {
  IconData icons;
  Function onPressed;
  FloatingBtnSuccess({this.icons, this.onPressed});

  @override
  Widget build(BuildContext context) {
    return FloatingActionButton(
      child: Icon(this.icons),
      backgroundColor: Colors.green,
      onPressed: this.onPressed,
    );
  }
}
