import 'package:flutter/material.dart';
import 'package:flutter_template/screen/form/addorder.screen.dart';
import 'package:flutter_template/ui/button/ButtonPrimary.dart';
import 'package:flutter_template/ui/button/FloatingBtnSuccess.dart';
import 'package:flutter_template/ui/widget/HeaderApp.dart';
import 'package:flutter_template/ui/widget/MenuListTile.dart';
import 'package:get/get.dart';

class MenuScreen extends StatefulWidget {
  @override
  _MenuScreenState createState() => _MenuScreenState();
}

class _MenuScreenState extends State<MenuScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        shrinkWrap: true,
        slivers: [
          SliverAppBar(
            // title: Text("MENU UTAMA"),
            floating: true,
            flexibleSpace: FlexibleSpaceBar(
              background: HeaderApp(),
            ),
            // flexibleSpace: Placeholder(),
            expandedHeight: 300,
          ),
          SliverList(
            delegate: SliverChildListDelegate(
              [
                MenuListTile(
                  onPress: () {

                  },
                  iconName: 'Mock',
                  title: 'Mock Exam',
                  subTitle: '24 Practise questions to prepare you for the test',
                ),
                MenuListTile(
                  onPress: () {

                  },
                  iconName: 'Practice',
                  title: 'Practice Questions',
                  subTitle: '1,000+ Questions to practice',
                ),
                MenuListTile(
                  onPress: () {

                  },
                  iconName: 'Revision',
                  title: 'Revision',
                  subTitle: 'All the reading material needed for the test',
                ),
                MenuListTile(
                  onPress: () {

                  },
                  iconName: 'Tips',
                  title: 'Tips to pass the test',
                  subTitle: 'Helpful tips to help you pass the exam',
                ),
                MenuListTile(
                  onPress: () {},
                  iconName: 'Rate',
                  title: 'Rate Us',
                ),
              ],
            )
          )
        ],
      ),
      floatingActionButton: FloatingBtnSuccess(
        icons: Icons.add,
        onPressed: (){
            Get.to(AddOrderScreen());
        },
      )
    );
  }
}
