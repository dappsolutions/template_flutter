import 'package:flutter/material.dart';
import 'package:flutter_template/ui/form/InputText.dart';

class AddOrderScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Add Order"),
        elevation: 0,
      ),
      body: Container(
        padding: EdgeInsets.all(16),
        child: Column(
          children: [
            InputText()
          ],
        ),
      ),
    );
  }
}
